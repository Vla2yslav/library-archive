﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

public class Data_Controller : MonoBehaviour
{
    [Header("Database")]
    [SerializeField] private Library_DataBase dataBase;

    [Header("Add Section")]
    [SerializeField] private InputField _idInput;
    [SerializeField] private InputField _authorInput;
    [SerializeField] private InputField _nameInput;
    [SerializeField] private InputField _countryInput;
    [SerializeField] private InputField _genreInput;
    [SerializeField] private InputField _yearInput;
    [SerializeField] private InputField _countInput;
    [SerializeField] private InputField _countPageInput;

    [Header("Delete Section")]
    [SerializeField] private InputField _idDeleteInput;

    [Header("Events")]
    public UnityEvent OnAddElement = new UnityEvent();
    public UnityEvent OnDeleteElement = new UnityEvent();

    public void AddToDB()
    {
        if (_idInput.text != "" && _authorInput.text != "" && _nameInput.text != "" && _countInput.text != "" &&
        _genreInput.text != "" && _yearInput.text != "" && _countInput.text != "" && _countPageInput.text != "")
        {
            ImportInfo _import = new ImportInfo();
            _import.ID = int.Parse(_idInput.text);
            _import.Author = _authorInput.text;
            _import.Name = _nameInput.text;
            _import.Country = _countInput.text;
            _import.Genre = _genreInput.text;
            _import.Year = int.Parse(_yearInput.text);
            _import.Count = int.Parse(_countInput.text);
            _import.PageCount = int.Parse(_countPageInput.text);

            try
            {
                dataBase.Add(_import);
                OnAddElement?.Invoke();
            }
            catch
            {
                Debug.Log("Error");
            }
        }
        else
        {
            Debug.Log("Fill all fields.");
        }
    }
    public void RemoveFromDB()
    {
        if (_idDeleteInput.text != "")
        {
            try
            {
                dataBase.Delete(int.Parse(_idDeleteInput.text));
                OnDeleteElement?.Invoke();
            }
            catch
            {
                Debug.Log("Error");
            }
        }
        else
        {
            Debug.Log("Fill empty field.");
        }
    }
}