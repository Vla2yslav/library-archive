﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ImportInfo")]
public class ImportInfo : ScriptableObject
{
    public int ID;
    public string Author;
    public string Name;
    public string Country;
    public string Genre;
    public int Year;
    public int Count;
    public int PageCount;
}