﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LibraryDatabase")]
public class Library_DataBase : ScriptableObject
{

    public List<ImportInfo> Database
    {
        get { return _database; }
    }
    private List<ImportInfo> _database = new List<ImportInfo>();
    public void Add(ImportInfo import)
    {
        AssetDatabase.CreateAsset(import, "Assets/Database/" + import.Name+".asset");
        _database.Add(import);
    }
    public void Delete(int index)
    {
        AssetDatabase.DeleteAsset("Assets/Database/" + _database[index].Name+".asset");
        _database.RemoveAt(index);
    }
}