﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class Logger : MonoBehaviour
{
    private TextAsset _log;
    private string _pathLog = "Assets/Logs/Log.txt";
    private void Awake()
    {
        _log = AssetDatabase.LoadAssetAtPath(_pathLog, typeof(TextAsset)) as TextAsset;
        if (_log == null)
        {
            AssetDatabase.CreateAsset(_log, _pathLog);
        }
    }
    public void LogEvent(string line)
    {
        string _logFile = File.ReadAllText(_pathLog);
        _logFile += DateTime.Now + ": Log event( " + line + " )" + "\n";
        File.WriteAllText(_pathLog, _logFile);
        AssetDatabase.Refresh();
    }
}
