﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableBuilder : MonoBehaviour
{
    [SerializeField] private Library_DataBase _libraryDatabase;
    [SerializeField] private GameObject _cellPrefab;
    [SerializeField] private GameObject _rowPrefab;
    private List<GameObject> _rows = new List<GameObject>();

    private void Awake()
    {
        for (int i = 0; i < _libraryDatabase.Database.Count; i++)
        {
            CreateRow(_libraryDatabase.Database[i]);
        }
    }
    private void CreateRow(ImportInfo importInfo)
    {
        GameObject _row = Instantiate(_rowPrefab, transform, false);
        _rows.Add(_row);
        Text _cellText = _cellPrefab.GetComponentInChildren<Text>();
        _cellText.text = importInfo.ID.ToString();
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Author;
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Name;
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Country;
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Genre;
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Year.ToString();
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.Count.ToString();
        Instantiate(_cellPrefab, _row.transform, false);
        _cellText.text = importInfo.PageCount.ToString();
        Instantiate(_cellPrefab, _row.transform, false);
    }
    public void AddElement()
    {
        CreateRow(_libraryDatabase.Database[_libraryDatabase.Database.Count - 1]);
    }
    public void DeleteElement()
    {
        for (int i = 0; i < _rows.Count; i++)
        {
            Destroy(_rows[i]);
        }
        _rows.Clear();

        for (int i = 0; i < _libraryDatabase.Database.Count; i++)
        {
            CreateRow(_libraryDatabase.Database[i]);
        }
    }
}
